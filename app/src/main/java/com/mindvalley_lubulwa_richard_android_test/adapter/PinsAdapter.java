package com.mindvalley_lubulwa_richard_android_test.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.lubulwa.imageloader.Handler;
import com.lubulwa.imageloader.cache.CacheController;
import com.mindvalley_lubulwa_richard_android_test.R;
import com.mindvalley_lubulwa_richard_android_test.models.Pin;
import com.mindvalley_lubulwa_richard_android_test.utilities.Utilities;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by lubulwa on 11/27/16.
 */
public class PinsAdapter extends RecyclerView.Adapter<PinsAdapter.ViewHolder> implements View.OnClickListener {

    private Context mContext;
    private Utilities utilities;
    private List<Pin> pins;

    private int lastPos = -1;

    public PinsAdapter(Context mContext_, Utilities utilities, List<Pin> pins){
        this.mContext = mContext_;
        this.utilities = new Utilities(mContext_);
        this.pins = pins;
    }
    @Override
    public PinsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pin_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PinsAdapter.ViewHolder holder, int position) {
        int total_images = 0;
        Pin pins = this.pins.get(position);
        if (pins.getUser() != null){
            holder.name.setText(pins.getUser().getName());
            holder.user_name.setText("@" + pins.getUser().getUsername());
            holder.likes.setText(String.valueOf(pins.getLikes()) + " likes");
        }
        CacheController.getInstance(mContext).getResourceManager().grabResource(pins.getUrls().getSmall(), new Handler() {
            @Override
            public void fromRemote(Call<ResponseBody> call, Response<ResponseBody> response, byte[] bytes) {
                setPinImage(holder, bytes);
            }

            @Override
            public void fromCache(byte[] bytes) {
                setPinImage(holder, bytes);
            }

            @Override
            public void fromLocal(byte[] bytes) {

            }
        }, holder.cancelLoading);

        utilities.grabResource(pins.getUser().getProfileImage().getSmall(), new Handler() {
            @Override
            public void fromRemote(Call<ResponseBody> call, Response<ResponseBody> response, byte[] bytes) {
                setProfilePic(holder, bytes);
            }

            @Override
            public void fromCache(byte[] bytes) {
                setProfilePic(holder, bytes);
            }

            @Override
            public void fromLocal(byte[] bytes) {

            }
        });

        holder.itemView.setTag(pins);

    }

    private void setProfilePic(ViewHolder holder, byte[] bytes) {
        Bitmap image = CacheController.getInstance(mContext).getConverters().convertBytestoBitmap(bytes);
        holder.profilePic.setImageBitmap(image);
    }

    private void setPinImage(ViewHolder holder, byte[] bytes) {
        holder.cancelLoading.setVisibility(View.GONE);
        Bitmap bitmap = CacheController.getInstance(mContext).getConverters().convertBytestoBitmap(bytes);
        Animation myFadeInAnimation = AnimationUtils.loadAnimation(holder.itemView.getContext(), R.anim.fade_in);
        holder.pinImage.startAnimation(myFadeInAnimation);
        holder.pinImage.setImageBitmap(bitmap);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public ImageView pinImage;
        public ImageButton cancelLoading;
        public ImageView profilePic;
        public TextView name, user_name, likes;

        public ViewHolder(View itemView) {
            super(itemView);
            pinImage = (ImageView) itemView.findViewById(R.id.pin_image);
            cancelLoading = (ImageButton) itemView.findViewById(R.id.cancel_loading);
            profilePic = (ImageView) itemView.findViewById(R.id.profile_pic);
            name = (TextView) itemView.findViewById(R.id.name);
            user_name = (TextView) itemView.findViewById(R.id.user_name);
            likes = (TextView) itemView.findViewById(R.id.likes);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }

    @Override
    public int getItemCount() {
        return this.pins.size();
    }

    @Override
    public void onClick(View v) {

    }

}

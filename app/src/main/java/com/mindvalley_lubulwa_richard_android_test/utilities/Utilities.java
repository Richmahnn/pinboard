package com.mindvalley_lubulwa_richard_android_test.utilities;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lubulwa.imageloader.Handler;
import com.lubulwa.imageloader.cache.CacheController;
import com.mindvalley_lubulwa_richard_android_test.models.Pin;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by lubulwa on 11/27/16.
 */
public class Utilities {
    private Context context;
    private Gson gson;
    public Handler handler = null;
    CacheController.ResourceManager resourceManager;
    public Utilities(Context context) {
        this.context= context;
        this.gson=new Gson();
        resourceManager=CacheController.getInstance(this.context).getResourceManager();
    }
    public List<Pin> deserializeToBoards(String serialized){
        Type type = new TypeToken<List<Pin>>() {
        }.getType();
        return  gson.fromJson(serialized, type);
    }
    public void grabResource(String url, final Handler handler) {
        resourceManager.grabResource(url,handler,null);
    }
    public void grabResource(int resId, final Handler handler) {
        resourceManager.grabResource(resId,handler,null);
    }
}

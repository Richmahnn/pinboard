package com.mindvalley_lubulwa_richard_android_test;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.lubulwa.imageloader.Handler;
import com.lubulwa.imageloader.cache.CacheController;
import com.mindvalley_lubulwa_richard_android_test.adapter.PinsAdapter;
import com.mindvalley_lubulwa_richard_android_test.models.Pin;
import com.mindvalley_lubulwa_richard_android_test.utilities.Utilities;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView pinsRecyclerView;
    private ProgressBar progressBar;
    private Utilities utilities;
    private StaggeredGridLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        initialize();
        loadPins();
    }

    private void initialize(){
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);
        pinsRecyclerView = (RecyclerView) findViewById(R.id.pins_recycler_view);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        utilities = new Utilities(MainActivity.this);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadPins();
                Toast.makeText(MainActivity.this, "Refreshing pins", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadPins(){
        utilities.grabResource("http://pastebin.com/raw/wgkJgazE", new Handler() {
            @Override
            public void fromRemote(Call<ResponseBody> call, Response<ResponseBody> response, byte[] bytes) {
                List<Pin> pins = utilities.deserializeToBoards(CacheController.getInstance(getApplicationContext()).getConverters().convertBytestoUtf8(bytes));
                AttachDataToRecycler(pins);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void fromCache(byte[] bytes) {
                List<Pin> pins = utilities.deserializeToBoards(CacheController.getInstance(getApplicationContext()).getConverters().convertBytestoUtf8(bytes));
                AttachDataToRecycler(pins);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void fromLocal(byte[] bytes) {

            }
        });
    }

    private void AttachDataToRecycler(List<Pin> pins) {
        pinsRecyclerView.setHasFixedSize(true);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            mLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
            pinsRecyclerView.setLayoutManager(mLayoutManager);

        } else {
            mLayoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
            pinsRecyclerView.setLayoutManager(mLayoutManager);

        }
        PinsAdapter pinsAdapter = new PinsAdapter(getApplicationContext(), utilities, pins);
        pinsRecyclerView.setAdapter(pinsAdapter);
        if (swipeRefreshLayout.isRefreshing()){
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

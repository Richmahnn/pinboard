package com.mindvalley_lubulwa_richard_android_test.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by lubulwa on 11/27/16.
 */
public class Category {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("photo_count")
    @Expose
    private String photoCount;

    @SerializedName("links")
    @Expose
    private Links1 links;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhotoCount() {
        return photoCount;
    }

    public void setPhotoCount(String photoCount) {
        this.photoCount = photoCount;
    }

    public Links1 getLinks() {
        return links;
    }

    public void setLinks(Links1 links) {
        this.links = links;
    }
}

package com.lubulwa.imageloader;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by lubulwa on 11/26/16.
 */
public interface Handler {
    public void fromRemote(Call<ResponseBody> call, Response<ResponseBody> response, byte[] bytes);
    public void fromCache(byte[] bytes);
    public void fromLocal(byte[] bytes);
}

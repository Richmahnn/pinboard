package com.lubulwa.imageloader;

/**
 * Created by lubulwa on 11/26/16.
 */
public class UriScheme {
    public String url;
    public String contentType;

    public UriScheme(String url, String contentType){
        this.url = url;
        this.contentType = contentType;
    }
}
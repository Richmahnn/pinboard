package com.lubulwa.imageloader;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

/**
 * Created by lubulwa on 11/26/16.
 */
public class ServiceBuilder {
    private static Retrofit.Builder builder;
    public static final String API_BASE_URL = "http://your.api-base.url";
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    public ServiceBuilder(){
        InitRetrofitBuilder();
    }
    public void InitRetrofitBuilder(){
        builder= new Retrofit.Builder().baseUrl(API_BASE_URL).addCallAdapterFactory(RxJavaCallAdapterFactory.create());
    }
    public static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }
}

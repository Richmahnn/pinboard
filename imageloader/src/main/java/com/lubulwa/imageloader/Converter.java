package com.lubulwa.imageloader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.UnsupportedEncodingException;

/**
 * Created by lubulwa on 11/27/16.
 */
public class Converter {
    public Bitmap convertBytestoBitmap(byte[] bytes){
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }
    public String convertBytestoUtf8(byte[] bytes){
        try {
            String str = new String(bytes, "UTF-8");
            return str;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
}

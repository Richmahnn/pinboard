package com.lubulwa.imageloader;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by lubulwa on 11/26/16.
 */
public interface RetrofitApi {
    @GET
    Call<ResponseBody> fetchResource(@Url String url);
}

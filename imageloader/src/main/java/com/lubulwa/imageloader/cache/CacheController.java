package com.lubulwa.imageloader.cache;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import android.util.LruCache;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lubulwa.imageloader.Converter;
import com.lubulwa.imageloader.Handler;
import com.lubulwa.imageloader.RetrofitApi;
import com.lubulwa.imageloader.ServiceBuilder;
import com.lubulwa.imageloader.UriScheme;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lubulwa on 11/26/16.
 */
public class CacheController {
    private int maximumMemory;
    private Context mContext;
    private Gson gson;
    private static CacheController instance;
    private LruCache<String, byte[]> mMemoryCache;
    private Converter converter;
    private static ResourceManager resourceManager;

    public CacheController(Context mContext){
        gson = new Gson();
        this.mContext = mContext;
        this.maximumMemory = (int) ((Runtime.getRuntime().maxMemory() / 1024) / 6);
        this.converter = new Converter();
        initLruCache(maximumMemory);
    }

    public static CacheController getInstance(Context context) {
        if (instance == null) {
            return instance = new CacheController(context);
        }
        return instance;
    }

    public Converter getConverters(){
        return this.converter;
    }

    public LruCache<String, byte[]> getLruCache(){
        return mMemoryCache;
    }

    private void initLruCache(int maxMemory) {
        mMemoryCache=new LruCache<String, byte[]>(maxMemory){
        };
    }

    public String cacheKeyFromUrl(String url){
        String cacheKey = null;
        Map<String, byte[]> mSnapShot = mMemoryCache.snapshot();
        for (String id : mSnapShot.keySet()){
            Type mType = new TypeToken<UriScheme>(){}.getType();
            UriScheme uriScheme = gson.fromJson(id, mType);
            if (uriScheme.url.equals(url)){
                cacheKey = id;
                break;
            }
        }
        return cacheKey;
    }

    public void storeToMemoryCacheWithKey(String key, byte[] bytes) {
        mMemoryCache.put(key, bytes);
    }

    public byte[] loadCacheFromMemoryWithKey(String caheKey) {
        byte[] bytes = mMemoryCache.get(caheKey);
        if (bytes != null) {
        } else {
        }
        return bytes;
    }

    public byte[] loadCacheFromMemory(String url){
        String cacheKey = cacheKeyFromUrl(url);
        if (TextUtils.isEmpty(cacheKey)){
            return null;
        }
        return loadCacheFromMemoryWithKey(cacheKey);
    }
    public void storeToMemoryCache(String url, byte[] bytes){
        storeToMemoryCacheWithKey(url, bytes);
    }

    public void clearMemoryCache(){
        mMemoryCache.evictAll();
    }

    public ResourceManager getResourceManager(){
        if(resourceManager==null)
            return resourceManager= new ResourceManager();
        return resourceManager;
    }

    public class ResourceManager{

        public Call grabResourceFromRemote(String url, final Handler handler, View cancel){
            ServiceBuilder builder=new ServiceBuilder();
            RetrofitApi api= builder.createService(RetrofitApi.class);
            final Call<ResponseBody> call= api.fetchResource(url);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    byte[] bytes=  updateCache(call,response);
                    handler.fromRemote(call, response,bytes);
                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e("error", t.getMessage());
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
            if(cancel!=null) {
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        call.cancel();
                    }
                });
            }
            return call;
        }

        private void grabResourceFromLocal(int resId, Handler handler) {
            Drawable r= mContext.getResources().getDrawable(resId);
            Bitmap bitmap = ((BitmapDrawable)r).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
            byte[] bytes = stream.toByteArray();
            UriScheme scheme=new UriScheme(String.valueOf(resId),"drawable");
            storeToMemoryCache(gson.toJson(scheme), bytes);
            handler.fromLocal(bytes);
        }

        public byte[] grabResourceFromCache(String uri){
            byte[] bytes= loadCacheFromMemory(uri);
            return bytes;
        }


        public void grabResource(String uri, final Handler handler, View cancel) {
            byte[] bytes= grabResourceFromCache(uri);
            if(bytes!=null) {
                handler.fromCache(bytes);
                return;
            }
            grabResourceFromRemote(uri, handler, cancel);
        }

        public void grabResource(int resource, final Handler handler, View cancel) {
            byte[] bytes= grabResourceFromCache(String.valueOf(resource));
            if(bytes!=null) {
                handler.fromCache(bytes);
                return;
            }
            grabResourceFromLocal(resource,handler);
        }

        public byte[] updateCache(Call<ResponseBody> call, Response<ResponseBody> response) {
            byte[] bytes=null;
            String uri=  call.request().url().toString();
            String contentType= response.headers().get("content-type");
            UriScheme scheme= new UriScheme(uri,contentType);
            String serializedScheme= gson.toJson(scheme);
            try {
                bytes= response.body().bytes();
                storeToMemoryCache(serializedScheme, bytes);
                Log.i("log", serializedScheme);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bytes;
        }
    }

}
